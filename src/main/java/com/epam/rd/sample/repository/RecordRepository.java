package com.epam.rd.sample.repository;

import com.epam.rd.sample.model.Record;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RecordRepository extends JpaRepository<Record, Long> {
}
