package com.epam.rd.sample.controller;

import com.epam.rd.sample.model.Record;
import com.epam.rd.sample.repository.RecordRepository;
import java.util.ArrayList;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/record")
public class RecordController {

    private final RecordRepository recordRepository;

    @Autowired
    public RecordController(RecordRepository recordRepository) {
        this.recordRepository = recordRepository;
    }

    @PostMapping
    public void saveRecord(@RequestBody Record record) {
        recordRepository.save(record);
    }

    @GetMapping("/{id}")
    public Record getRecord(@PathVariable(name = "id") Long id) {
        return recordRepository.findById(id)
                .orElse(null);
    }

    @GetMapping
    public Collection<Record> getAllRecords() {
        return new ArrayList<>(recordRepository.findAll());
    }
}